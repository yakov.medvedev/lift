package model;

/**
 * Created by medvedevyakov on 2019-09-22.
 */
public class Passenger {

    private String name;
    private int entryFloor;
    private int neededFloor;

    public Passenger() {
    }

    public Passenger(String name) {
        this.name = name;
    }

    public Passenger(String name, int entryFloor, int neededFloor) {
        this.name = name;
        this.entryFloor = entryFloor;
        this.neededFloor = neededFloor;
    }

    public String getName() {
        return name;
    }

    public int getEntryFloor() {
        return entryFloor;
    }

    public int getNeededFloor() {
        return neededFloor;
    }
}
