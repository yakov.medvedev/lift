package utility;

import model.Passenger;

/**
 * Created by medvedevyakov on 2019-09-23.
 */
public class Message {

    public static void doorOpen(){
        System.out.println("Doors open.");
    }

    public static void comeIn(Passenger p){
        System.out.println("Passenger " + p.getName() + " went into the lift.");
    }

    public static void comeOut(Passenger p){
        System.out.println("Passenger " + p.getName() + " went out the lift.");
    }

    public static void doorClose(){
        System.out.println("Doors are closing.");
    }

    public static void buttonClick(int number){
        System.out.println("Button \"" + number + " floor\" clicked.");
    }

    public static void currentFloor(int number){
        System.out.println("Now is " + number + " flour.");    }
}
