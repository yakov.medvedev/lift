package service;

import model.Passenger;
import utility.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by medvedevyakov on 2019-09-22.
 */
public class Lift {

    public static int Floor_ONE = 1;
    public static int Floor_TWO = 2;
    public static int Floor_THREE = 3;
    public static int Floor_FOUR = 4;

    private int currentFloor = 1;
    private List<Passenger> passengersInLift = new ArrayList();
    private List<Passenger> passengersInQueue = new ArrayList();

    public Lift() {
    }

    public void callTheLift(Passenger p){
        passengersInQueue.add(p);
    }

    public void addPassenger(Passenger p){
        Message.doorOpen();
        Message.comeIn(p);
        passengersInLift.add(p);
        passengersInQueue.remove(p);
        Message.buttonClick(p.getNeededFloor());
        Message.doorClose();
    }

    public void outPassenger(Passenger p){
        Message.doorOpen();
        passengersInLift.remove(p);
        Message.comeOut(p);
        Message.doorClose();
    }

    public void move() {
        for (int i = 1; i < 5; i++) {
            currentFloor = i;
            Message.currentFloor(i);
            for (Passenger p : passengersInQueue) {
                if (p.getEntryFloor() == currentFloor){
                    addPassenger(p);
                }
                if (p.getNeededFloor() == currentFloor){
                    outPassenger(p);
                }
            }
            System.out.println();
        }

        for (int i = 4; i > 0; i--) {
            currentFloor = i;
            Message.currentFloor(i);
            for (Passenger p : passengersInQueue) {
                if (p.getEntryFloor() == currentFloor){
                    addPassenger(p);
                }
                if (p.getNeededFloor() == currentFloor){
                    outPassenger(p);
                }
            }
        }
    }
}
