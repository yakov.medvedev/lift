import model.Passenger;
import service.Lift;

/**
 * Created by medvedevyakov on 2019-09-22.
 */
public class Main {

    public static void main(String[] args) {

        Lift lift = new Lift();

        Passenger p1 = new Passenger("Vasya",Lift.Floor_ONE, Lift.Floor_FOUR);
        Passenger p2 = new Passenger("Sara",Lift.Floor_THREE, Lift.Floor_TWO);
        Passenger p3 = new Passenger("Bobby",Lift.Floor_FOUR, Lift.Floor_ONE);
        lift.callTheLift(p1);
        lift.callTheLift(p2);
        lift.callTheLift(p3);
        lift.move();
    }
}
